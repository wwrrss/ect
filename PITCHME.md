## Hola!
### Spring Boot + Angular. 
#### Una breve introducción.
---
### Qué es Spring Boot ? 

# 'Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run"'.

Note:
-Introducción Spring Boot 

---
### Pre-requisitos
Note: 
- Instalación Java 8 
- Instalación Mavan
---

### Pre-requisitos Angular
## node js
## npm
## typescript 

### Angular 

Note:
- https://cli.angular.io/
- npm install -g @angular/cli
- ng new ect-front
- cd ect-front
- ng serve
---
### Plugins de VS Code Para Angular 
## John Pappa

### Modulos en Angular 
## App Module 

### Componentes en Angular 
---
Note:
- Crear componente con ng generate c mi-componente

### Agregar Bootstrap a Angular 

Note:
- npm install bootstrap
- @import '~bootstrap/dist/css/bootstrap.min.css';
---
### Servicios en Angular 

Note:
- Crear servicio con ng-generate s mi-servicio
---
### HttpClient 

Note:
- Agregar HttpClientModule en el root AppModule (import {HttpClientModule} from '@angular/common/http';)
---
### Binding 
## ngFor
# for loop 
## Event Binding 
# Bind de eventos como click, focus, blur etc
## Template reference variable 
# Es una referencia a un elemento del DOM 

Note: 
- *ngFor="let d of posts"
- (click)="onClickMethod()"
- (click)="onClickMethod($event)"
- Template refence #miVar en el elemento 
---
### Formularios
## Directiva #varForm="ngForm"
## ngModel 
## Validaciones 
## Input y OutPut

Note:
- Agregar forms modules en App module
- FormsModule import { FormsModule }   from '@angular/forms';
---

## The End :)
